                                
                                        ---------Pre-install //Centos ---------
sudo dnf update -y
sudo systemctl stop firewalld
sudo systemctl disable firewalld

 

                                        ---------k3s install---------
curl -sfL https://get.k3s.io | sh -s - --no-deploy traefik --write-kubeconfig-mode 644 --node-name k3s-master

join worker nodes:

cat /var/lib/rancher/k3s/server/node-token

WORKER 
curl -sfL https://get.k3s.io | K3S_NODE_NAME=k3s-worker-04 K3S_URL=https://master_ip:6443 
K3S_TOKEN= sh - 


                                        ---------k3s dashboard On master node---------
GITHUB_URL=https://github.com/kubernetes/dashboard/releases
VERSION_KUBE_DASHBOARD=$(curl -w '%{url_effective}' -I -L -s -S ${GITHUB_URL}/latest -o /dev/null | sed -e 's|.*/||')
sudo k3s kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/${VERSION_KUBE_DASHBOARD}/aio/deploy/recommended.yaml


         ////create : dashboard.admin-user.yml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kubernetes-dashboard

        ////create : dashboard.admin-user-role.yml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kubernetes-dashboard


            kubectl create -f dashboard.admin-user.yml -f dashboard.admin-user-role.yml

Obtain token
 kubectl -n kubernetes-dashboard create token admin-user


Access : http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes/dashboard:/proxy/



                                        ---------NGINX ingress controller---------

kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.2.0/deploy/static/provider/cloud/deploy.yaml