
                     ---------mysql client---------
yum install mysql


                     ---------vtctlclient---------
version=13.0.0
file=vitess-${version}-bc4a960.tar.gz
wget https://github.com/vitessio/vitess/releases/download/v${version}/${file}
tar -xzf ${file}
cd ${file/.tar.gz/}
sudo mkdir -p /usr/local/vitess
sudo cp -r * /usr/local/vitess/
export PATH=/usr/local/vitess/bin:${PATH}


                    ---------vitess---------
git clone https://github.com/vitessio/vitess.git
cd vitess/examples/operator
kubectl apply -f operator.yaml


./pf.sh &
alias vtctlclient="vtctlclient --server=localhost:15999"
alias mysql="mysql -h 127.0.0.1 -P 15306 -u user"


vtctlclient ApplySchema --sql="$(cat create_commerce_schema.sql)" commerce
vtctlclient ApplyVSchema --vschema="$(cat vschema_commerce_initial.json)" commerce
